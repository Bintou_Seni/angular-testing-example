import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { BadMouseComponent } from './bad-mouse/bad-mouse.component';
import { MouseComponent } from './mouse/mouse.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent, BadMouseComponent, MouseComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
